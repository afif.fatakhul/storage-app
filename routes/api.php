<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AduanController;
use App\Http\Controllers\KTPController;
use App\Http\Controllers\PoliController;
use App\Http\Controllers\DokterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('upload-video-aduan', [AduanController::class, 'saveVideo']);
Route::post('upload-foto-aduan', [AduanController::class, 'saveFoto']);
Route::post('upload-foto-ktp', [KTPController::class, 'saveFoto']);
Route::post('upload-foto-dokter', [DokterController::class, 'saveFotoDokter']);
Route::post('upload-foto-ruangan', [PoliController::class, 'saveFoto']);
