<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AduanController extends Controller
{
    
    public function saveVideo(Request $request)
    {
        $path = public_path().'/informasi-rs/fileaduans/'. $_FILES['filedata']['name'];
	    move_uploaded_file($_FILES['filedata']['tmp_name'], $path); 
        return;
    }

    public function saveFoto(Request $request)
    {
        $folderPath = "informasi-rs/fileaduans/";
        $image_base64 = base64_decode($request->link_file);
        $fileName = uniqid().'.'.$request->tipe_file;
        $file = $folderPath . $fileName;
        file_put_contents($file, $image_base64);
        return response()->json(["code" => 200, "message" => $fileName, "success" => true]);
    }

}
