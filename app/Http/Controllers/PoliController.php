<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PoliController extends Controller
{
    public function saveFoto(Request $request)
    {
        $folderPath = "informasi-rs/poli/";
        $image_base64 = base64_decode($request->fotoRuangan);
        $fileName = uniqid().'.'.$request->tipe_file;
        $file = $folderPath . $fileName;
        file_put_contents($file, $image_base64);
        return response()->json(["code" => 200, "message" => $fileName, "success" => true]);
    }
}
