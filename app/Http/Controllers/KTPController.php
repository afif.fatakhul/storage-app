<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class KTPController extends Controller
{
    
    public function saveFoto(Request $request)
    {
        $folderPath = "informasi-rs/ktp/";
        $image_base64 = base64_decode($request->file);
        $fileName = uniqid().'.png';
        $file = $folderPath . $fileName;
        file_put_contents($file, $image_base64);
        return response()->json(["code" => 200, "message" => $fileName, "success" => true]);
    }

}
