<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DokterController extends Controller
{
    
    public function saveFotoDokter(Request $request)
    {
        $path = public_path().'/informasi-rs/dokter/'. $_FILES['filedata']['name'];
	    move_uploaded_file($_FILES['filedata']['tmp_name'], $path); 
        return;
    }

}
